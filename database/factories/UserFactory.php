<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(User::class, function (Faker $faker) {

    #ENCRIPTAMOS EL LA CONTRASEÑA
    #PARA ASIGNARLE A TODOS LOS USUARIOS
    $hashed_password = Hash::make('123456');

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => $hashed_password,
        'remember_token' => Str::random(10),
    ];
});
