<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Song;
use Faker\Generator as Faker;

$factory->define(Song::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence(3),#se requiere 3 palabras
        'artist_group'=>$faker->sentence(2),#se requiere 2 palabras
        'release_date'=>$faker->dateTimeBetween('-1 month', '+1 month'),#fecha de entre el mes pasado y un mes posterior
        'track'=>$faker->sentence(4),#se requiere 4 palabras
        'cover'=>$faker->imageUrl(50,50),#se solicita una imagen de un acho de 50px y 50px de alto

    ];
});
