<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        #SE INVOCA A LA SEMBRADORA DE SONGS
        $this->call(SongSeeder::class);
    }
}
