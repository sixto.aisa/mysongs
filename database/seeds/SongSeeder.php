<?php

use Illuminate\Database\Seeder;

class SongSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #SE CREAN 10 USUARIOS
        $users=factory(\App\User::class,10)->create()->each(function ($user){
            #POR CADA USUARIO SE CREAN 5 CANCIONES
            $user->songs()->createMany(
                factory(\App\Song::class,5)->make()->toArray()
            );
        });
    }
}
