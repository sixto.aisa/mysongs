<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Song extends Model
{

    #SEA ESTABLECIDO UNA RELACIÓN DE UNO A UNO CON EL MODELO USER
    #UNA CANCION TIENE UN USUARIO
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
