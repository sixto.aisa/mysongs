<?php

namespace App\Http\Controllers;

use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SongController extends Controller
{
    #CONSTRUCTOR
    public function __construct()
    {
        #SE ASIGNA EL MIDDLEWARE DE AUTORIZACION SOLO PARA LA FUNCION INDEX
        $this->middleware('auth')->only('index');
    }

    #FUNCION INDEX
    public function index()
    {
        #OBTENEMOS EL ID DE LA SESION DEL USUARIO AUTENTICADO
        $user_id=Auth::id();
        #FILTRO DE LAS CANCIONES DEL USUARIO
        $songs=Song::where('user_id','=',$user_id)->get();
        #INVOCAMOS A LA VISTA INDEX ENVIANDOLE LA VARIABLE SONGS
        return view('index',compact('songs'));
    }

}
