<?php

namespace App\Http\Livewire;

use App\Song;
use Livewire\Component;

class Counter extends Component
{
    public $message ;
    public $data;

    public function mount()
    {
        $this->data=array();
        $this->message = 'Hello World!';
    }

    public function render()
    {
        return view('livewire.counter');
    }

    public function addLike()
    {

        $song= new Song();
        $song->cancion=$this->message;
        array_push($this->data,$song);
    }
}
