<!--SE EXTIENDE DE LA PLANTILLA PRINCIPAL APP.BLADE.PHP -->
@extends('layouts.app')
<!--SE DECLARA LA SECCION DE CONTENIDO -->
@section('content')
    @livewire('counter')

    <div class="container">
        <!-- SE RECIBE LA VARIABLE SONGS DESDE EL CONTROLADOR -->
        @foreach($songs as $song)
            <!-- SE GENERA UN CARD POR CADA CANCION MEDIANTE UN BUCLE -->
            <div class="row mb-4 justify-content-md-center">
                <div class="col-md-6">
                    <!-- SE MUESTRAN LOS ATRIBUTOS DEL OBJETO SONG -->
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title">{{$song->title}}</h2>
                            <h5 class="card-subtitle mb-2 text-muted">{{$song->artist_group}}</h5>
                            <p class="card-text">{{$song->track}}</p>
                        </div>
                        <img src="{{asset($song->cover)}}"  alt="...">
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection





